import 'dart:convert' as convert;
import 'package:http/http.dart' as http;

class NetworkHelper {
  static Future getData(String url) async {
    http.Response response = await http.get(url);
    var jsonResponse = convert.jsonDecode(response.body);
    if (response.statusCode == 200) {
      return jsonResponse;
    } else if (response.statusCode >= 400 && response.statusCode < 500) {
      print("Request failed with status: ${response.statusCode}. with $url");
      throw jsonResponse['message'];
    } else {
      print('Server Failed');
    }
  }
}
