import 'dart:ffi';

import 'package:geolocator/geolocator.dart';
import 'networking.dart';
import 'package:clima/utilities/constants.dart';

class WeatherData {
  final int condition;
  final LocationData location;
  final double temperature;
  WeatherData({this.condition, this.location, this.temperature});

  static WeatherData decode(dynamic jsonResponse) {
    var city = jsonResponse['city']['name'];
    double temperature = jsonResponse['list'][0]['temp']['day'];
    double celsius = temperature - 273.15;
    int condition = jsonResponse['list'][0]['weather'][0]['id'];
    var lat = jsonResponse['city']['lat'];
    var lon = jsonResponse['city']['lon'];
    Position position = Position(
        latitude: double.parse(lat.toString()),
        longitude: double.parse(lon.toString()));
    LocationData locationData = LocationData(city.toString(), position);
    return WeatherData(
        condition: condition, temperature: celsius, location: locationData);
  }
}

class LocationData {
  final String city;
  final Position position;

  LocationData(this.city, this.position);
}

class WeatherModel {
  String getWeatherIcon(int condition) {
    if (condition < 300) {
      return '🌩';
    } else if (condition < 400) {
      return '🌧';
    } else if (condition < 600) {
      return '☔️';
    } else if (condition < 700) {
      return '☃️';
    } else if (condition < 800) {
      return '🌫';
    } else if (condition == 800) {
      return '☀️';
    } else if (condition <= 804) {
      return '☁️';
    } else {
      return '🤷‍';
    }
  }

  String getMessage(int temp) {
    if (temp > 25) {
      return 'It\'s 🍦 time';
    } else if (temp > 20) {
      return 'Time for shorts and 👕';
    } else if (temp < 10) {
      return 'You\'ll need 🧣 and 🧤';
    } else {
      return 'Bring a 🧥 just in case';
    }
  }
}

class WeatherAPIService {
  Future<WeatherData> getWeatherData({Position p}) async {
    String lat = p.latitude.toString();
    String lon = p.longitude.toString();
//    String url = 'https://api.openweathermap.org/data/2.5/forecast/daily?lat=$lat&lon=$lon&appid=$kWeatherAPIId';

    String url =
        "https://samples.openweathermap.org/data/2.5/forecast/daily?lat=35&lon=139&cnt=10&appid=b1b15e88fa797225412429c1c50c122a1";
    try {
      var jsonResponse = await NetworkHelper.getData(url);
      return WeatherData.decode(jsonResponse);
    } catch (e) {
      throw e;
    }
  }

  Future<WeatherData> getWeatherDataWithCity({String city}) async {
    String url =
        "https://samples.openweathermap.org/data/2.5/forecast/daily?q=$city&units=metric&cnt=7&appid=b1b15e88fa797225412429c1c50c122a1";
    try {
      var jsonResponse = await NetworkHelper.getData(url);
      return WeatherData.decode(jsonResponse);
    } catch (e) {
      throw e;
    }
  }
}
