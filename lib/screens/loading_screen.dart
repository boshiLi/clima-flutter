import 'package:clima/screens/location_screen.dart';
import 'package:clima/services/location.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:clima/services/weather.dart';

class LoadingScreen extends StatefulWidget {
  @override
  _LoadingScreenState createState() => _LoadingScreenState();
}

class _LoadingScreenState extends State<LoadingScreen> {
  WeatherData weather;
  String loadingMessage = 'Loading Weather Data';

  void prepareToPushWeatherPage() async {
    Position currentLocation;
    try {
      currentLocation = await LocationHelper().getCurrentLocation();
    } catch (e) {
      handleErrorMessage(e);
    }
    try {
      WeatherData weather =
          await WeatherAPIService().getWeatherData(p: currentLocation);
      Route route = MaterialPageRoute(
        builder: (context) => LocationScreen(
          weather: weather,
        ),
      );
      Navigator.push(context, route);
    } catch (e) {
      this.handleErrorMessage(e);
    }
  }

  void handleErrorMessage(e) {
    setState(() {
      this.loadingMessage = e.toString();
    });
  }

  @override
  void initState() {
    super.initState();
    this.prepareToPushWeatherPage();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SpinKitRotatingCircle(
            color: Colors.white,
            size: 50.0,
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            this.loadingMessage,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 20,
            ),
          ),
        ],
      ),
    );
  }
}
