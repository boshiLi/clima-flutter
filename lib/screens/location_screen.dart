import 'package:clima/screens/city_screen.dart';
import 'package:clima/services/weather.dart';
import 'package:flutter/material.dart';
import 'package:clima/utilities/constants.dart';

class LocationScreen extends StatefulWidget {
  final WeatherData weather;
  LocationScreen({this.weather});

  @override
  _LocationScreenState createState() => _LocationScreenState();
}

class _LocationScreenState extends State<LocationScreen> {
  WeatherData weather;
  String degree;
  String weatherMessage;
  @override
  void initState() {
    this.updateUI(widget.weather);
    super.initState();
  }

  void fetchWeatherData() async {
    var data = await WeatherAPIService()
        .getWeatherData(p: this.widget.weather.location.position);
    this.updateUI(data);
  }

  void fetchCityWeatherData(String city) async {
    var data = await WeatherAPIService().getWeatherDataWithCity(city: city);
    this.updateUI(data);
  }

  void updateUI(WeatherData data) {
    setState(() {
      if (data != null) {
        this.weather = data;
        this.degree = '${data.temperature.round()}°';
        this.weatherMessage =
            '${WeatherModel().getMessage(this.weather.temperature.round())} in ${data.location.city}!';
      } else {
        this.degree = '--';
        this.weatherMessage = 'Unable To Fetch Weather Data...😭';
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('images/location_background.jpg'),
            fit: BoxFit.cover,
            colorFilter: ColorFilter.mode(
                Colors.white.withOpacity(0.8), BlendMode.dstATop),
          ),
        ),
        constraints: BoxConstraints.expand(),
        child: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  FlatButton(
                    onPressed: () {
                      this.fetchWeatherData();
                    },
                    child: Icon(
                      Icons.near_me,
                      size: 50.0,
                    ),
                  ),
                  FlatButton(
                    onPressed: () async {
                      Route route = MaterialPageRoute(
                        builder: (context) => CityScreen(),
                      );
                      var city = await Navigator.push(context, route);
                      if (city != null) {
                        this.fetchCityWeatherData(city);
                      }
                    },
                    child: Icon(
                      Icons.location_city,
                      size: 50.0,
                    ),
                  ),
                ],
              ),
              Padding(
                padding: EdgeInsets.only(left: 15.0),
                child: Row(
                  children: <Widget>[
                    Text(
                      '${this.weather.temperature.round()}°',
                      style: kTempTextStyle,
                    ),
                    Text(
                      WeatherModel().getWeatherIcon(this.weather.condition),
                      style: kConditionTextStyle,
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(right: 15.0),
                child: Text(
                  this.weatherMessage,
                  textAlign: TextAlign.right,
                  style: kMessageTextStyle,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
