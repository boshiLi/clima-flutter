void main() {
  perform();
}

void perform() async {
  t1();
  String b2 = await t2();
  t3(b2);
}

void t1() {
  print('t1');
}

Future<String> t2() async {
  Duration duration = Duration(seconds: 2);

  String result;
  await Future.delayed(duration, () {
    result = 't2';
  });
  return result;
}

void t3(String t2) {
  print(t2);
}
